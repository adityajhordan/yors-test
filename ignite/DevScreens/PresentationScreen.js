import React from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity } from 'react-native'
import { Images } from './DevTheme'
import ButtonBox from './ButtonBox'
import { StackNavigator, TabNavigator } from 'react-navigation'
// Screens
import APITestingScreen from './APITestingScreen'
import ComponentExamplesScreen from './ComponentExamplesScreen'
import DeviceInfoScreen from './DeviceInfoScreen'
import PluginExamplesScreen from './PluginExamplesScreen'
import ThemeScreen from './ThemeScreen'
import FaqScreen from './FaqScreen'
import QrScreen from './QrScreen'
import TabNavigationRoot from './TabNavigationRoot'

// Styles
import styles from './Styles/PresentationScreenStyles'

class PresentationScreen extends React.Component {

  openComponents = () => {
    this.props.navigation.navigate('ComponentExampleRoot')
  }

  openUsage = () => {
    console.log('trigering');
    this.props.navigation.navigate('PluginExamplesScreen')
  }

  openApi = () => {
    this.props.navigation.navigate('APITestingRoot')
  }

  openTheme = () => {
    this.props.navigation.navigate('ThemeRoot')
  }

  openDevice = () => {
    this.props.navigation.navigate('DeviceInfoRoot')
  }

  openFaq = () => {
    this.props.navigation.navigate('FaqRoot')
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <TouchableOpacity onPress={this.props.screenProps.toggle} style={{
          position: 'absolute',
          paddingTop: 30,
          paddingHorizontal: 10,
          zIndex: 10
        }}>
          <Image source={Images.closeButton} />
        </TouchableOpacity>
        <ScrollView showsVerticalScrollIndicator={false} bounces={false} style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.igniteClear} style={styles.logo} />
          </View>

          <Text style={styles.sectionText}>
            Default screens for development, debugging, and alpha testing
            are available below.
          </Text>
          <View style={styles.buttonsContainer}>
            <ButtonBox onPress={this.openComponents} style={styles.componentButton} image={Images.components} text='Components' />
            <ButtonBox onPress={this.openUsage} style={styles.usageButton} image={Images.usageExamples} text='Plugin Examples' />
          </View>
          <View style={styles.buttonsContainer}>
            <ButtonBox onPress={this.openApi} style={styles.apiButton} image={Images.api} text='API Testing' />
            <ButtonBox onPress={this.openTheme} image={Images.theme} text='Theme' />
          </View>
          <View style={styles.buttonsContainer}>
            <ButtonBox onPress={this.openDevice} style={styles.deviceButton} image={Images.deviceInfo} text='Device Info' />
            <ButtonBox onPress={this.openFaq} style={styles.usageButton} image={Images.faq} text='FAQ' />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const PresentationScreenRoot = StackNavigator({
  PresentationScreen: {
    screen: PresentationScreen,
    navigationOptions: {
      header:null
    }
  },
  PluginExamplesScreen: {
    screen: PluginExamplesScreen,
    navigationOptions: {
      header:null
    }
  },
})


const FaqRoot = StackNavigator({
  FaqScreen : { 
    screen: FaqScreen,
    navigationOptions: {
      header:null
    }
  },
});

const QrScanRoot = StackNavigator({
  FaqScreen : { 
    screen: QrScreen,
    navigationOptions: {
      header:null
    }
  },
});

const ThemeRoot = StackNavigator({
  ThemeScreen : { 
    screen: ThemeScreen,
    navigationOptions: {
      header:null
    }
  }
});

const ComponentExampleRoot = StackNavigator({
  ComponentExamplesScreen : { 
    screen: ComponentExamplesScreen,
    navigationOptions: {
      header:null
    }
  }
});

const APITestingRoot = StackNavigator({
  APITestingScreen : { 
    screen: APITestingScreen,
    navigationOptions: {
      header:null
    }
  }
});

const DeviceInfoRoot = StackNavigator({
  DeviceInfoScreen : { 
    screen: DeviceInfoScreen,
    navigationOptions: {
      header:null
    }
  }
});

export default TabNavigator({
  PresentationScreenRoot: { screen: PresentationScreenRoot },
  APITestingRoot: { screen: APITestingRoot },
  DeviceInfoRoot: { screen : DeviceInfoRoot},
  ComponentExampleRoot: { screen : ComponentExampleRoot},
  ThemeRoot: { screen : ThemeRoot},
  FaqRoot: { screen : FaqRoot},
  QrScanRoot: { screen : QrScanRoot},
},
{
  tabBarComponent: TabNavigationRoot,
  tabBarPosition: 'bottom',
});
