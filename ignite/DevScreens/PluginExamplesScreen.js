// Fair Warning:  PluginExamples has a good bit of Ignite automation in editing.
// Though robust, if you should modify this file, review your changes with us
// As to not break the automated addition/subtractions.
import React from 'react'
import { View, ScrollView, Text, TouchableOpacity, Image, Platform } from 'react-native'
import { StackNavigator } from 'react-navigation'
import { Images } from './DevTheme'

// Examples Render Engine
import ExamplesRegistry from '../../App/Services/ExamplesRegistry'
import '../Examples/Components/MapsExample.js'
import '../Examples/Components/animatableExample.js'
import '../Examples/Components/i18nExample.js'
import '../Examples/Components/vectorExample.js'

// Styles
import styles from './Styles/PluginExamplesScreenStyles'

export default class PluginExamplesScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      isPlatformAndroid : false,
      isPlatformIos : false
    };
  }

  componentDidMount(){
    this.checkOs();
  }

  checkOs(){
    console.log('checking OS: ', Platform.OS);
    if(Platform.OS == 'ios'){
      
      this.setState({
        isPlatformIos : true
      });
   
    }else{
     
      this.setState({
        isPlatformAndroid : true
      });
    
    }
  }

  render () {
    console.log('value of OS IOS: ', this.state.isPlatformIos);
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <TouchableOpacity onPress={() => this.props.navigation.navigate('PresentationScreen')} style={{
          position: 'absolute',
          paddingTop: 30,
          paddingHorizontal: 5,
          zIndex: 10
        }}>
          <Image source={Images.backButton} />
        </TouchableOpacity>
        <ScrollView style={styles.container}>
          <View style={{alignItems: 'center', paddingTop: 60}}>
            <Image source={Images.usageExamples} style={styles.logo} />
            <Text style={styles.titleText}>Plugin Examples</Text>
          </View>
          <View style={styles.section}>
            <Text style={styles.sectionText} >
              The Plugin Examples screen is a playground for 3rd party libs and logic proofs.
              Items on this screen can be composed of multiple components working in concert.  Functionality demos of libs and practices
            </Text>
          </View>

          {this.state.isPlatformIos &&
            ExamplesRegistry.renderPluginExamples()
          }

          {!this.state.isPlatformIos &&
            <Text
              style={{color:'white', alignSelf:'center'}}  
            >
            Sorry this feature doesn't suppor on Android OS.</Text>
          }

          <View style={styles.screenButtons} />

        </ScrollView>
      </View>
    )
  }
}

