/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Button,
  Image
} from 'react-native';
import { Images } from './DevTheme'
import styles from './Styles/PresentationScreenStyles'
import ButtonBox from './ButtonBox'
import { StackNavigator, TabNavigator } from 'react-navigation';


type Props = {};
export default class TabNavigatorRoot extends Component<Props> {
  
  constructor(props) {
    super(props);
    
    this.state = {
      isSubMenuDetailShow : true
    };

  }

  navigateApiScreen(){
    this.props.navigation.navigate('APITestingRoot');
  }

  navigateDeviceInformationScreen(){
    this.props.navigation.navigate('DeviceInfoRoot'); 
  }

  navigateComponentScreen(){
    this.props.navigation.navigate('ComponentExampleRoot');
  }

  navigatePresentationScreen(){
    this.props.navigation.navigate('PresentationScreenRoot');
  }

  navigatePluginScreen(){
   this.props.navigation.navigate('PluginExamplesScreen'); 
  }

  navigateThemeScreen(){
    this.props.navigation.navigate('ThemeRoot');  
  }

  navigateFaqScreen(){
    this.props.navigation.navigate('FaqRoot');   
  }

  navigateQrScanScreen(){
    this.props.navigation.navigate('QrScanRoot');   
  }

  render() {
    return (
      <View>
        
        {!this.state.isSubMenuDetailShow &&
          <View style={{height: 70, flexDirection:'row', alignItems:'center', borderTopWidth: 2, borderTopColor:'gray'}}>
            {
              //Component Section
            }
            <TouchableHighlight 
              style={{flex:0.25, alignItems:'center',backgroundColor: '#3C243C', height: 70, justifyContent:'center'}}
              onPress={()=>this.navigateComponentScreen()}
            >
              <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-components.png')}/>
            </TouchableHighlight>

            {
              //Plugin Section
            }
            <TouchableHighlight 
              style={{flex:0.25, alignItems:'center',backgroundColor: '#3C243C', height: 70, justifyContent:'center'}}
              onPress={()=>this.navigatePluginScreen()}
            >
              <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-usage-examples.png')}/>
            </TouchableHighlight>

            {
              //Api Section
            }
            <TouchableHighlight 
              style={{flex:0.25, alignItems:'center',backgroundColor: '#3C243C', height: 70, justifyContent:'center'}}
              onPress={()=>this.navigateApiScreen()}
            >
              <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-api-testing.png')}/>
            </TouchableHighlight>

            {
              //Sub Category Section
            }
            <TouchableHighlight 
              style={{flex:0.25, alignItems:'center',backgroundColor: '#3C243C', height: 70, justifyContent:'center'}}
              onPress={() => this.setState({isSubMenuDetailShow: true})}
            >
              <Image style={{flex:1 }} resizeMode="stretch" source={require('./Images/Icons/arrow_up.png')}/>
            </TouchableHighlight>
          </View>
        }

        {this.state.isSubMenuDetailShow &&
          <View style={{height: 120, flexDirection:'row', borderTopWidth: 2, borderTopColor:'gray'}}>
            
            {
              //Column 1 Component & Device
            }
            <View style={{height: 120, flex:0.25, flexDirection:'column'}}>
              {
                //Component Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateComponentScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-components.png')}/>
              </TouchableHighlight>

              {
                //Device Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateDeviceInformationScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-device-information.png')}/>
              </TouchableHighlight>
            </View>

            {
              //Column 2 Plugin & Theme
            }
            <View style={{height: 120, flex:0.25, flexDirection:'column'}}>
              {
                //Plugin Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigatePluginScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-usage-examples.png')}/>
              </TouchableHighlight>

              {
                //Theme Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateThemeScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-theme.png')}/>
              </TouchableHighlight>
            </View>

            {
              //Column 3 API and FAQ
            }

            <View style={{height: 120, flex:0.25, flexDirection:'column'}}>
              {
                //API Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateApiScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/icon-api-testing.png')}/>
              </TouchableHighlight>

              {
                //FAQ Button Screen
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateFaqScreen()}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/faq-icon.png')}/>
              </TouchableHighlight>
            </View>

            {
              //Column 4 QR code & !isSubMenuDetailShow
            }
            <View style={{height: 120, flex:0.25, flexDirection:'column'}}>
              {
                //QR subMenuDetail
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={()=>this.navigateQrScanScreen()}
              >
                <Image style={{flex:0.75, resizeMode:'stretch' }} resizeMode="stretch" source={require('./Images/Icons/qr-code.png')}/>
              </TouchableHighlight>

              {
                //hidden subMenuDetail
              }
              <TouchableHighlight 
              style={{flex:0.5, alignItems:'center',backgroundColor: '#3C243C', justifyContent:'center'}}
              onPress={() => this.setState({isSubMenuDetailShow: false})}
              >
                <Image style={{flex:0.75 }} resizeMode="stretch" source={require('./Images/Icons/arrow_bottom.png')}/>
              </TouchableHighlight>
            </View>

          </View>
        }
      </View>
    );
  }
}

